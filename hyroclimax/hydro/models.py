from django.db import models

from django.db import models


class Images(models.Model):
    image_field = models.ImageField(upload_to='images/')


# Home page
class Header(models.Model):
    text = models.CharField(max_length=400)
    heading = models.CharField(max_length=100)
    background_image = models.ForeignKey(Images, on_delete=models.CASCADE)


class OurAim(models.Model):
    body = models.TextField(max_length=400)
    link = models.CharField(max_length=300)
    images = models.ManyToManyField(Images)


# HydroclimaxCafe
class Crouser(models.Model):
    heading = models.CharField(max_length=200)
    body = models.CharField(max_length=400)
    image = models.ForeignKey(Images, on_delete=models.CASCADE)
    link = models.CharField(max_length=300)


subject_choice = [("intern", "Internship"),
                  ("phd", "PHD"),
                  ("expert-feedback", "Expert Feedback"),
                  ("joint-paper-writing", "Joint Paper Writing"),
                  ("exchange", "Excahnge")]


class Intern(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=300)
    country = models.CharField(max_length=100)

    subject = models.CharField(default='intern', choices=subject_choice,max_length=200)
    description = models.TextField(max_length=500)


class WorkExperience(models.Model):
    title = models.CharField(max_length=200)
    from_date = models.DateField()
    end_date = models.DateField()
    heading = models.CharField(max_length=200)
    body = models.CharField(max_length=400)


class Profile(models.Model):
    photo = models.ForeignKey(Images, on_delete=models.CASCADE)
    about = models.TextField(max_length=500)
    experience = models.ManyToManyField(WorkExperience)
    heading = models.CharField(max_length=200)


class Education(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    from_date = models.DateField()
    end_date = models.DateField()
    degree = models.CharField(max_length=200)
    body = models.CharField(max_length=300)
    college_name = models.CharField(max_length=200)


class AwardsAndAchievements(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date = models.DateField()
    heading = models.CharField(max_length=200)
    body = models.CharField(max_length=300)
    subject = models.CharField(max_length=200)


class OtherProfile(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    from_date = models.DateField()
    end_date = models.DateField()
    subject = models.CharField(max_length=200)
    body = models.CharField(max_length=400)


class PersonalInfo(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    date_of_birth = models.DateField()
    address = models.CharField(max_length=400)
    email = models.CharField(max_length=200)
    number = models.CharField(max_length=50)
    skype = models.CharField(max_length=200)
    connected = models.DateField()
    website = models.CharField(max_length=300)


class Website(models.Model):
    name = models.CharField(max_length=200)
    link = models.CharField(max_length=300)


class SocialMedia(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    website = models.ManyToManyField(Website)


class ListResearchDomain(models.Model):
    list = models.CharField(max_length=200)


# ResearchFocus
class CurrentResearch(models.Model):
    body = models.CharField(max_length=200)
    image = models.ForeignKey(Images, on_delete=models.CASCADE)
    list_of_domains = models.ManyToManyField(ListResearchDomain)


class DataMiningList(models.Model):
    text = models.CharField(max_length=200)
    link = models.CharField(max_length=300)


class DataMining(models.Model):
    image = models.ForeignKey(Images, on_delete=models.CASCADE)
    body = models.ManyToManyField(DataMiningList)


# Resources
class Publication(models.Model):
    heading = models.CharField(max_length=200)
    meta_data = models.CharField(max_length=400)
    year_created = models.IntegerField()
    link = models.CharField(max_length=300)


class Blogs(models.Model):
    heading = models.CharField(max_length=200)
    body = models.CharField(max_length=500)
    year_created = models.IntegerField()
    link = models.CharField(max_length=300)


class UdanList(models.Model):
    lectures = models.CharField(max_length=300)


class ShortTutorial(models.Model):
    link = models.CharField(max_length=300)


class UdanLecture(models.Model):
    heading = models.CharField(max_length=200)
    lecture_list = models.ManyToManyField(UdanList)
    tutorial_list = models.ManyToManyField(ShortTutorial)


class Team(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ForeignKey(Images, on_delete=models.CASCADE)
    description = models.CharField(max_length=400)
    type_of_member = models.CharField(max_length=100)
    year = models.IntegerField()
    institute = models.CharField(max_length=200)
    keyword = models.CharField(max_length=200)
