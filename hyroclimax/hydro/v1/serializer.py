from rest_framework import serializers

from hyroclimax.hydro.models import *


class ImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Images
        fields = ["id"]


class WebsiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Website
        fields = '__all__'


class SocialMediaSerializer(serializers.ModelSerializer):
    website = WebsiteSerializer(read_only=True, many=True)

    class Meta:
        model = SocialMedia
        fields = ['website']


class WorkExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkExperience
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    experience = WorkExperienceSerializer(read_only=True, many=True)

    class Meta:
        model = Profile
        fields = ['photo', 'about', 'experience']


class PersonalInfoSerializer(serializers.ModelSerializer):
    profile_id = serializers.IntegerField(source='profile.id')

    class Meta:
        model = PersonalInfo
        fields = ['first_name', 'last_name', 'date_of_birth', 'address', 'email', 'number', 'skype',
                  'connected', 'website', 'profile_id']


class EducationSerializer(serializers.ModelSerializer):
    profile_id = serializers.IntegerField(source='profile.id')

    class Meta:
        model = Education
        fields = ['title', 'from_date', 'end_date', 'degree', 'body', 'college_name', 'profile_id']


class AwardsSerializer(serializers.ModelSerializer):
    profile_id = serializers.IntegerField(source='profile.id')

    class Meta:
        model = AwardsAndAchievements
        fields = ['heading', 'date', 'body', 'subject', 'profile_id']


class OtherSerializer(serializers.ModelSerializer):
    profile_id = serializers.IntegerField(source='profile.id')

    class Meta:
        model = OtherProfile
        fields = ['title', 'from_date', 'end_date', 'body', 'subject', 'profile_id']


class HeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Header
        fields = ['text', 'heading', 'background_image']


class OurAimSerializer(serializers.ModelSerializer):
    images = ImagesSerializer(read_only=True, many=True)

    class Meta:
        model = OurAim
        fields = ['body', 'link', 'images']


class CrouserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Crouser
        fields = ['heading', 'body', 'link', 'image']


class InternSerializer(serializers.ModelSerializer):
    class Meta:
        model = Intern
        fields = '__all__'


class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = '__all__'


class BlobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blogs
        fields = '__all__'


class UdanListSerializer(serializers.ModelSerializer):
    class Meta:
        model = UdanList
        fields = '__all__'


class ShortTutorialSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShortTutorial
        fields = '__all__'


class UdanLectureSerializer(serializers.ModelSerializer):
    lecture_list = UdanListSerializer(many=True, read_only=True)
    tutorial_list = ShortTutorialSerializer(many=True, read_only=True)

    class Meta:
        model = UdanLecture
        fields = ["heading", "tutorial_list", "lecture_list"]


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['name', 'description', 'type_of_member', 'year', 'institute', 'keyword', 'photo']


class ResearchList(serializers.ModelSerializer):
    class Meta:
        model = ListResearchDomain
        fields = '__all__'


class CurrentResearchSerializer(serializers.ModelSerializer):
    list_of_domains = ResearchList(many=True, read_only=True)

    class Meta:
        model = CurrentResearch
        fields = ['body', 'image', 'list_of_domains']


class DataMiningList(serializers.ModelSerializer):
    class Meta:
        model = DataMiningList
        fields = '__all__'


class DataMiningSerializer(serializers.ModelSerializer):
    body = DataMiningList(many=True, read_only=True)

    class Meta:
        model = DataMining
        fields = ['image', 'body']
