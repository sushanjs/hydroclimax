from rest_framework import viewsets

from hyroclimax.hydro.models import *
from hyroclimax.hydro.v1.serializer import *


class SocialMediaViewset(viewsets.ModelViewSet):
    queryset = SocialMedia.objects.all()
    serializer_class = SocialMediaSerializer


class ProfileViewset(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class PersonalInfoViewset(viewsets.ModelViewSet):
    queryset = PersonalInfo.objects.all()
    serializer_class = PersonalInfoSerializer


class EducationViewset(viewsets.ModelViewSet):
    queryset = Education.objects.all()
    serializer_class = EducationSerializer


class AwardViewset(viewsets.ModelViewSet):
    queryset = AwardsAndAchievements.objects.all()
    serializer_class = AwardsSerializer


class OtherProfileViewset(viewsets.ModelViewSet):
    queryset = OtherProfile.objects.all()
    serializer_class = OtherSerializer


class HomeViewset(viewsets.ModelViewSet):
    queryset = Header.objects.select_related('background_image')
    serializer_class = HeaderSerializer


class OurAimViewset(viewsets.ModelViewSet):
    queryset = OurAim.objects.all()
    serializer_class = OurAimSerializer


class CrouserViewset(viewsets.ModelViewSet):
    queryset = Crouser.objects.select_related('image')
    serializer_class = CrouserSerializer


class InternViewset(viewsets.ModelViewSet):
    queryset = Intern.objects.all()
    serializer_class = InternSerializer


class PublicationViewset(viewsets.ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer


class BlobViewset(viewsets.ModelViewSet):
    queryset = Blogs.objects.all()
    serializer_class = BlobSerializer


class LectureViewset(viewsets.ModelViewSet):
    queryset = UdanLecture.objects.all()
    serializer_class = UdanLectureSerializer


class TeamViewset(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer


class CurrentResearchViewset(viewsets.ModelViewSet):
    queryset = CurrentResearch.objects.all()
    serializer_class = CurrentResearchSerializer


class DataMiningViewset(viewsets.ModelViewSet):
    queryset = DataMining.objects.all()
    serializer_class = DataMiningSerializer
