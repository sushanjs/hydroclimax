from django.urls import path

from hyroclimax.hydro.v1.viewsets import *
app_name = "hydro"
urlpatterns = [
    # Profile
    path('social', SocialMediaViewset.as_view({'get': 'list'}), name='social'),
    path('profile', ProfileViewset.as_view({'get': 'list'}), name='profile'),
    path('profile-info', PersonalInfoViewset.as_view({'get': 'list'}), name='info'),
    path('profile-education', EducationViewset.as_view({'get': 'list'}), name='education'),
    path('profile-award', AwardViewset.as_view({'get': 'list'}), name='award'),
    path('profile-other', OtherProfileViewset.as_view({'get': 'list'}), name='other'),
    # Homepage
    path('homepage-home', HomeViewset.as_view({'get': 'list'}), name='home'),
    path('homepage-aim', OurAimViewset.as_view({'get': 'list'}), name='aim'),
    path('homepage-cafe', CrouserViewset.as_view({'get': 'list'}), name='cafe'),
    path('homepage-intern', InternViewset.as_view({'get': 'list'}), name='intern'),
    # Resources
    path('resource-publication', PublicationViewset.as_view({'get': 'list'}), name='publication'),
    path('resource-blogs', BlobViewset.as_view({'get': 'list'}), name='blogs'),
    path('resource-lecture', LectureViewset.as_view({'get': 'list'}), name='lecture'),
    path('resource-team', TeamViewset.as_view({'get': 'list'}), name='team'),
    # Research
    path('research-current', CurrentResearchViewset.as_view({'get': 'list'}), name='lecture'),
    path('research-data-mining', DataMiningViewset.as_view({'get': 'list'}), name='team'),
]
