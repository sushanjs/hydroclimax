from django.contrib import admin
from .models import *


@admin.register(SocialMedia)
class SocialMediaAdmin(admin.ModelAdmin):
    pass


@admin.register(Website)
class WebsiteAdmin(admin.ModelAdmin):
    pass


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(WorkExperience)
class ProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(Images)
class ImagesAdmin(admin.ModelAdmin):
    pass


@admin.register(PersonalInfo)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Education)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(AwardsAndAchievements)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(OurAim)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(OtherProfile)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Intern)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Crouser)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Header)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(CurrentResearch)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(DataMiningList)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(DataMining)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Publication)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Blogs)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(UdanList)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(ShortTutorial)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(UdanLecture)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(Team)
class PersonalInfoAdmin(admin.ModelAdmin):
    pass


@admin.register(ListResearchDomain)
class ListResearchDomainAdmin(admin.ModelAdmin):
    pass
